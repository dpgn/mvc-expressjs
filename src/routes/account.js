var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Login', menuId: 'login' });
});

router.post('/', function(req, res, next) {
  const email = req.body['email'] || '';
  const password = req.body['password'] || '';
  console.log(`Here is email: ${email} and password: ${password}`)
  res.redirect(`/user?email=${email}`);
});

module.exports = router;
