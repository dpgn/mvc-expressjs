var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  //res.send('respond with a resource');
  const email = req.query.email || '';
  res.render('user', { title: 'User', menuId: 'user', email: email });
});

module.exports = router;
