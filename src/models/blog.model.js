const mongoose = require('mongoose');

const blogSchema = new mongoose.Schema({
    title: String,
    content: String,
    creationDate: Number
});

const Blog = mongoose.model('Blog', blogSchema, "Blog");

module.exports = Blog
