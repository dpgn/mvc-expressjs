var express = require('express');
var router = express.Router();

const blogService = require('../services/blog.service');

/* GET home page. */
router.get('/', async function(req, res, next) {
  let blogs = [];
  blogs = await blogService.getAllBlogs();
  res.render('index', { title: 'Home', menuId: 'home', blogs });
});

module.exports = router;
